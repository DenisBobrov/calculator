package org.home.calculator.function.factory;

import org.home.calculator.function.*;

import java.util.HashMap;
import java.util.Map;

public class DefaultFunctionFactory implements FunctionFactory {

    private Map<String, Function> functions;

    public DefaultFunctionFactory() {
        initializeFunctions();
    }

    @Override
    public Function createFunction(String name) {
        return functions.get(name);
    }

    private void initializeFunctions() {
        functions = new HashMap<String, Function>() {
            { put("sqrt", new SqrtFunction()); }
            { put("min", new MinFunction()); }
            { put("max", new MaxFunction()); }
            { put("sum", new SumFunction()); }
        };
    }
}
