package org.home.calculator.function;

import org.home.calculator.CalculationException;

public interface Function {
    public double calculate(Double... arguments) throws CalculationException;
}
