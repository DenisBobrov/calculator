package org.home.calculator.function;

import org.home.calculator.CalculationException;

import java.util.Arrays;
import java.util.Collections;

public class MaxFunction implements Function {

    @Override
    public double calculate(Double... arguments) throws CalculationException {
        if (arguments.length < 2) {
            throw new CalculationException(
                    String.format("Max function expects at least 2 (two) arguments. %d argument(s) given.", arguments.length)
            );
        }

        return Collections.max(Arrays.asList(arguments));
    }
}
