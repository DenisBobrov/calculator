package org.home.calculator.function.factory;

import org.home.calculator.function.Function;

public interface FunctionFactory {

    public Function createFunction(String name);
}
