package org.home.calculator.function;

import org.home.calculator.CalculationException;

public class SqrtFunction implements Function {

    @Override
    public double calculate(Double... arguments) throws CalculationException {
        if (arguments.length != 1) {
            throw new CalculationException(
                    String.format("Sqrt function expects only one argument. %s given.", arguments.length)
            );
        }

        if (arguments[0] < 0) {
            throw new CalculationException(
                    String.format("Cannot calculate sqrt of negative value %s.", arguments[0])
            );
        }

        return Math.sqrt(arguments[0]);
    }
}
