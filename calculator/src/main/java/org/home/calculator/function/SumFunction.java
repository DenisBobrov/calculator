package org.home.calculator.function;

import org.home.calculator.CalculationException;

public class SumFunction implements Function {

    @Override
    public double calculate(Double... arguments) throws CalculationException {
        if (arguments.length < 2) {
            throw new CalculationException(
                    String.format("Sum function expects at least 2 (two) arguments. %d argument(s) given.", arguments.length)
            );
        }

        double sum = 0;

        for (double argument : arguments) {
            sum += argument;
        }

        return sum;
    }
}
