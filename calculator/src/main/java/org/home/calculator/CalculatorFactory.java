package org.home.calculator;

public class CalculatorFactory {
    public static Calculator createMathCalculator() {
        return new MathCalculator();
    }
}
