package org.home.calculator;

import org.home.calculator.evaluation.EvaluationExpression;
import org.home.calculator.function.factory.DefaultFunctionFactory;
import org.home.calculator.parser.token.Tokenizer;
import org.home.calculator.parser.token.MathTokenizer;
import org.home.calculator.operation.factory.DefaultOperationFactory;
import org.home.calculator.parser.token.Token;
import org.home.calculator.parser.token.processor.MathTokenProcessor;
import org.home.calculator.parser.token.processor.TokenProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MathCalculator implements Calculator {

    private final Logger logger = LoggerFactory.getLogger(MathCalculator.class);

    @Override
    public double calculate(String expression) throws CalculationException {
        Token token;

        if (expression.isEmpty()) {
            if (logger.isWarnEnabled()) {
                logger.warn("Empty expression given");
            }

            throw new CalculationException("Empty expression given");
        }

        if (logger.isInfoEnabled()) {
            logger.info(String.format("Calculate expression '%s'", expression));
        }

        Tokenizer tokenizer = new MathTokenizer(expression);
        EvaluationExpression evaluationExpression = new EvaluationExpression(new DefaultOperationFactory(), new DefaultFunctionFactory());
        TokenProcessor tokenProcessor = new MathTokenProcessor(evaluationExpression);

        while (tokenizer.hasNextToken()) {
            tokenizer.nextToken().acceptTokenProcessor(tokenProcessor);
        }

        evaluationExpression.evaluate();

        if (evaluationExpression.isChildEvaluationContext()) {
            throw new CalculationException("Mismatched closing brackets.");
        }

        double result = evaluationExpression.getResult();

        if (logger.isInfoEnabled()) {
            logger.info(String.format("Result is '%s'", String.valueOf(result)));
        }

        return result;
    }
}
