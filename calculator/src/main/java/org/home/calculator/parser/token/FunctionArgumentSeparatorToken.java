package org.home.calculator.parser.token;

import org.home.calculator.parser.token.processor.TokenProcessor;

public class FunctionArgumentSeparatorToken extends Token {

    public FunctionArgumentSeparatorToken(String value, int startPosition) {
        super(value, startPosition);
    }

    @Override
    public void acceptTokenProcessor(TokenProcessor processor) {
        processor.process(this);
    }
}
