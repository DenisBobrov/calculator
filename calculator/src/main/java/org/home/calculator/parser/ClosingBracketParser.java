package org.home.calculator.parser;

import org.home.calculator.parser.token.ClosingBracketToken;
import org.home.calculator.stateMachine.math.MathState;
import org.home.calculator.parser.token.Token;

public class ClosingBracketParser implements Parser {

    private static final char CLOSING_BRACKET = ')';

    @Override
    public Token parse(int startPosition, String expression) throws ParseException {
        char in = expression.charAt(startPosition);

        if (in != CLOSING_BRACKET) {
            throw new ParseException(String.format("Cannot parse opening bracket. '%s' given.", in), startPosition);
        }

        return new ClosingBracketToken(String.valueOf(in), startPosition);

    }

    @Override
    public Enum getState() {
        return MathState.CLOSING_BRACKET;
    }
}
