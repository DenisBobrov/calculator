package org.home.calculator.parser;

import org.home.calculator.parser.token.NumberToken;
import org.home.calculator.stateMachine.math.MathState;
import org.home.calculator.parser.token.Token;

public class NumberParser implements Parser {

    @Override
    public Token parse(final int startPosition, String expression) {
        int position = startPosition;

        if (!isDigit(expression.charAt(position))) {
            throw new ParseException("Cannot parse digit.", position);
        }

        StringBuilder number = new StringBuilder();

        boolean isPointFound = false;

        while (position < expression.length()) {
            char in = expression.charAt(position);

            if (!isDigit(in)) {
                break;
            }

            if (in == '.') {
                if (!isPointFound) {
                    isPointFound = true;
                } else {
                    throw new ParseException("The digit cannot contain more the one point.", position);
                }
            }

            number.append(in);

            position++;
        }

        return new NumberToken(number.toString(), startPosition);
    }

    @Override
    public Enum getState() {
        return MathState.NUMBER;
    }

    private boolean isDigit(char in) {
        return Character.isDigit(in) || (in == '.');
    }
}
