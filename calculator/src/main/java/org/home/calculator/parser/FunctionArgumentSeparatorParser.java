package org.home.calculator.parser;

import org.home.calculator.parser.token.FunctionArgumentSeparatorToken;
import org.home.calculator.stateMachine.math.MathState;
import org.home.calculator.parser.token.Token;

public class FunctionArgumentSeparatorParser implements Parser {

    public static final char FUNCTION_SEPARATOR = ',';

    @Override
    public Token parse(int startPosition, String expression) throws ParseException {
        char in = expression.charAt(startPosition);

        if (in != FUNCTION_SEPARATOR) {
            throw new ParseException(String.format("Cannot parse function separator. '%s' given.", in), startPosition);
        }

        return new FunctionArgumentSeparatorToken(String.valueOf(in), startPosition);
    }

    @Override
    public Enum getState() {
        return MathState.FUNCTION_ARGUMENT_SEPARATOR;
    }
}
