package org.home.calculator.parser;

import org.home.calculator.parser.token.OpeningBracketToken;
import org.home.calculator.stateMachine.math.MathState;
import org.home.calculator.parser.token.Token;

public class OpeningBracketParser implements Parser {

    private static final char OPENING_BRACKET = '(';

    @Override
    public Token parse(int startPosition, String expression) throws ParseException {
        char in = expression.charAt(startPosition);

        if (in != OPENING_BRACKET) {
            throw new ParseException(String.format("Cannot parse opening bracket. '%s' given.", in), startPosition);
        }

        return new OpeningBracketToken(String.valueOf(in), startPosition);
    }

    @Override
    public Enum getState() {
        return MathState.OPENING_BRACKET;
    }
}
