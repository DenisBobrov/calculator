package org.home.calculator.parser;

public class ParseException extends RuntimeException {

    private int position;

    public ParseException(String message, int position) {
        super(message);

        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
