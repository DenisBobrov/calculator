package org.home.calculator.parser.token.processor;

import org.home.calculator.CalculationException;
import org.home.calculator.parser.token.*;

public interface TokenProcessor {

    public void process(NumberToken token) throws CalculationException;
    public void process(BinaryOperationToken token) throws CalculationException;
    public void process(ClosingBracketToken token) throws CalculationException;
    public void process(OpeningBracketToken token) throws CalculationException;
    public void process(FunctionToken token) throws CalculationException;
    public void process(FunctionArgumentSeparatorToken token) throws CalculationException;
}
