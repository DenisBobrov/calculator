package org.home.calculator.parser.token.processor;

import org.home.calculator.CalculationException;
import org.home.calculator.evaluation.EvaluationExpression;
import org.home.calculator.function.Function;
import org.home.calculator.operation.binary.BinaryOperation;
import org.home.calculator.parser.token.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MathTokenProcessor implements TokenProcessor {

    private Logger logger = LoggerFactory.getLogger(MathTokenProcessor.class);
    private EvaluationExpression evaluationExpression;

    public MathTokenProcessor(EvaluationExpression evaluationExpression) {
        this.evaluationExpression = evaluationExpression;
    }

    @Override
    public void process(NumberToken token) throws CalculationException {
        if (logger.isInfoEnabled()) {
            logger.info("Process number.");
        }

        double number = Double.parseDouble(token.getValue());

        evaluationExpression.getEvaluationContext().pushNumber(number);
    }

    @Override
    public void process(BinaryOperationToken token) throws CalculationException {
        if (logger.isInfoEnabled()) {
            logger.info("Process binary operation");
        }

        BinaryOperation binaryOperation = evaluationExpression.getOperationFactory().createBinaryOperation(
                token.getValue().charAt(0)
        );

        if (binaryOperation == null) {
            if (logger.isErrorEnabled()) {
                logger.error(String.format("Cannot recognize operation %s", token.getValue()));
            }

            throw new CalculationException(
                    String.format(
                            "Cannot recognize operation '%s' at position %d.",
                            token.getValue(),
                            token.getStartPosition()),
                    token.getStartPosition()
            );
        }

        evaluationExpression.addBinaryOperation(binaryOperation);
    }

    @Override
    public void process(ClosingBracketToken token) throws CalculationException {
        if (logger.isInfoEnabled()) {
            logger.info("Process right bracket");
        }

        if (!evaluationExpression.isChildEvaluationContext()) {
            if (logger.isErrorEnabled()) {
                logger.error("Mismatched closing brackets.");
            }

            throw new CalculationException(
                    String.format("Mismatched opening brackets at position %d", token.getStartPosition()),
                    token.getStartPosition()
            );
        }

        evaluationExpression.removeEvaluationContext();
    }

    @Override
    public void process(OpeningBracketToken token) throws CalculationException {
        if (logger.isInfoEnabled()) {
            logger.info("Process opening bracket.");
        }

        evaluationExpression.addEvaluationContext();
    }

    @Override
    public void process(FunctionToken token) throws CalculationException {
        Function function = evaluationExpression.getFunctionFactory().createFunction(token.getValue());

        if (function == null) {
            throw new CalculationException(
                    String.format("Unknown function '%s' at position %d.", token.getValue(), token.getStartPosition()),
                    token.getStartPosition()
            );
        }

        evaluationExpression.addFunction(function);
    }

    @Override
    public void process(FunctionArgumentSeparatorToken token) throws CalculationException {
        if (!evaluationExpression.getEvaluationContext().isFunctionContext()) {
            throw new CalculationException("Cannot use function separator in this context.", token.getStartPosition());
        }

        evaluationExpression.addFunctionArgument();
    }
}
