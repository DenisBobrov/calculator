package org.home.calculator.parser.token;

import org.home.calculator.parser.token.processor.TokenProcessor;

public abstract class Token {

    private String value;
    private int startPosition;

    public Token(String value, int startPosition) {
        this.value = value;
        this.startPosition = startPosition;
    }

    public String getValue() {
        return value;
    }

    public int getStartPosition() {
        return startPosition;
    }

    public abstract void acceptTokenProcessor(TokenProcessor processor);

    @Override
    public String toString() {
        return String.format("Token(value='%s',start='%d')", value, startPosition);
    }
}
