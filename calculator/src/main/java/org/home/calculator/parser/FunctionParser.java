package org.home.calculator.parser;

import org.home.calculator.parser.token.FunctionToken;
import org.home.calculator.stateMachine.math.MathState;
import org.home.calculator.parser.token.Token;

public class FunctionParser implements Parser {

    @Override
    public Token parse(int startPosition, String expression) throws ParseException {
        final int position = startPosition;

        if (!Character.isLowerCase(expression.charAt(position))) {
            throw new ParseException("Cannot parse function.", position);
        }

        return doParse(startPosition, expression);
    }

    @Override
    public Enum getState() {
        return MathState.FUNCTION;
    }

    private Token doParse(int startPosition, String expression) {
        int position = startPosition;
        final StringBuilder builder = new StringBuilder();

        builder.append(expression.charAt(position));

        position++;

        while (position < expression.length()) {
            char in = expression.charAt(position);

            if (!isFunctionChar(in)) {
                break;
            }

            builder.append(in);

            position++;
        }

        if (builder.length() < 2) {
            throw new ParseException("The function should be min of 2 (two) characters.", position);
        }

        return new FunctionToken(builder.toString(), startPosition);
    }

    private boolean isFunctionChar(char in) {
        return Character.isLetter(in) || Character.isDigit(in);
    }
}
