package org.home.calculator.parser.token;

import org.home.calculator.parser.token.Tokenizer;
import org.home.calculator.parser.CalculatorParserFacade;
import org.home.calculator.parser.Parser;
import org.home.calculator.parser.token.Token;

public class MathTokenizer implements Tokenizer {

    private String input;
    private int currentPosition;
    private Token currentToken;
    private Parser parser;

    public MathTokenizer(String input) {
        this.input = input;

        parser = new CalculatorParserFacade();
    }

    @Override
    public boolean hasNextToken() {
        boolean hasNextToken = currentPosition < input.length();

        if (hasNextToken) {
            char in = input.charAt(currentPosition);

            // @TODO: extract to method
            if (Character.isSpaceChar(in) || Character.isISOControl(in)) {
                currentPosition++;

                return hasNextToken();
            }
        }

        return hasNextToken;
    }

    @Override
    public Token nextToken() {
        currentToken = parser.parse(currentPosition, input);

        currentPosition += currentToken.getValue().length();

        return currentToken;
    }
}
