package org.home.calculator.parser.token;

import org.home.calculator.parser.token.Token;

public interface Tokenizer {

    public boolean hasNextToken();
    public Token nextToken();
}
