package org.home.calculator.parser;

import org.home.calculator.parser.token.BinaryOperationToken;
import org.home.calculator.stateMachine.math.MathState;
import org.home.calculator.parser.token.Token;

public class BinaryOperationParser implements Parser {

    @Override
    public Token parse(int startPosition, String expression) throws ParseException {
        char in = expression.charAt(startPosition);

        if (!isOperation(in)) {
            throw new ParseException(String.format("Could not recognize operation '%s.", in), startPosition);
        }

        return new BinaryOperationToken(String.valueOf(in), startPosition);
    }

    @Override
    public Enum getState() {
        return MathState.BINARY_OPERATION;
    }

    private boolean isOperation(char in) {
        int charType = Character.getType(in);

        if (in == FunctionArgumentSeparatorParser.FUNCTION_SEPARATOR) {
            return false;
        }

        return charType == Character.OTHER_PUNCTUATION
                || charType == Character.CURRENCY_SYMBOL
                || charType == Character.MODIFIER_SYMBOL
                || charType == Character.DASH_PUNCTUATION
                || charType == Character.CONNECTOR_PUNCTUATION
                || charType == Character.MATH_SYMBOL;
    }
}
