package org.home.calculator.parser.token;

import org.home.calculator.parser.token.processor.TokenProcessor;

public class FunctionToken extends Token {

    public FunctionToken(String value, int startPosition) {
        super(value, startPosition);
    }

    @Override
    public void acceptTokenProcessor(TokenProcessor processor) {
        processor.process(this);
    }
}
