package org.home.calculator.parser;

import org.home.calculator.parser.token.Token;

public interface Parser<State extends Enum> {

    public Token parse(int startPosition, String expression) throws ParseException;

    public State getState();
}
