package org.home.calculator.parser;

import org.home.calculator.stateMachine.math.MathFiniteStateMachine;
import org.home.calculator.stateMachine.FiniteStateMachine;
import org.home.calculator.stateMachine.InvalidStateException;
import org.home.calculator.parser.token.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.home.calculator.stateMachine.math.MathState.*;

public class CalculatorParserFacade implements Parser {

    private final Logger logger = LoggerFactory.getLogger(CalculatorParserFacade.class);

    private FiniteStateMachine stateMachine;
    private Map<Enum, Parser> parsers;

    public CalculatorParserFacade() {
        stateMachine = new MathFiniteStateMachine();

        parsers = new HashMap<Enum, Parser>() {
            { put(NUMBER, new NumberParser()); }
            { put(BINARY_OPERATION, new BinaryOperationParser()); }
            { put(OPENING_BRACKET, new OpeningBracketParser()); }
            { put(CLOSING_BRACKET, new ClosingBracketParser()); }
            { put(FUNCTION, new FunctionParser()); }
            { put(FUNCTION_ARGUMENT_SEPARATOR, new FunctionArgumentSeparatorParser()); }
        };
    }

    @Override
    public Token parse(final int startPosition, String expression) {
        Set<Enum> possibleStates = stateMachine.getTransitionMatrix().getPossibleStates(stateMachine.getCurrentState());
        Token token;

        for (Enum state : possibleStates) {
            Parser parser = parsers.get(state);

            if (logger.isInfoEnabled()) {
                logger.info(String.format("Try parse token with: %s", parser.getClass().toString()));
            }

            try {
                token = parser.parse(startPosition, expression);

                if (logger.isInfoEnabled()) {
                    logger.info(String.format("Parsed token: %s", token.toString()));
                }
            } catch (ParseException e) {
                if (logger.isInfoEnabled()) {
                    logger.info(String.format("Parsing fail: %s", e.getMessage()));
                }

                continue;
            }

            if (token != null) {
                stateMachine.acceptNewState(parser.getState());

                if ((token.getValue().length() + startPosition) == expression.length()) {
                    if (!stateMachine.isFiniteSate(parser.getState())) {
                        throw new InvalidStateException(String.format("Math expression cannot finish with '%s'", token.getValue()));
                    }
                }

                return token;
            }
        }

        String message = String.format(
                "Cannot parse token at position %d. Possible states are: %s", startPosition, possibleStates.toString()
        );

        if (logger.isErrorEnabled()) {
            logger.error(message);
        }

        throw new ParseException(message, startPosition);
    }

    @Override
    public Enum getState() {
        throw new IllegalStateException("State is not supported");
    }
}
