package org.home.calculator.operation.binary;

import org.home.calculator.operation.Operation;

public interface BinaryOperation extends Operation {

    public Association getAssociation();
    public int getPrecedence();
    public double calculate(double leftOperand, double rightOperand);
}
