package org.home.calculator.operation.binary;

import org.home.calculator.evaluation.OperationVisitor;

public class Multiplication implements BinaryOperation {

    @Override
    public Association getAssociation() {
        return Association.LEFT;
    }

    @Override
    public int getPrecedence() {
        return 2;
    }

    @Override
    public double calculate(double leftOperand, double rightOperand) {
        return leftOperand * rightOperand;
    }

    @Override
    public void accept(OperationVisitor visitor) {
        visitor.visit(this);
    }
}
