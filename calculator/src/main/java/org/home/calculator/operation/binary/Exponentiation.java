package org.home.calculator.operation.binary;

import org.home.calculator.evaluation.OperationVisitor;

public class Exponentiation implements BinaryOperation {

    @Override
    public Association getAssociation() {
        return Association.RIGHT;
    }

    @Override
    public int getPrecedence() {
        return 3;
    }

    @Override
    public double calculate(double leftOperand, double rightOperand) {
        return Math.pow(leftOperand, rightOperand);
    }

    @Override
    public void accept(OperationVisitor visitor) {
        visitor.visit(this);
    }
}
