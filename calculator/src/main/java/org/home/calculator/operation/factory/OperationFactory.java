package org.home.calculator.operation.factory;

import org.home.calculator.operation.binary.BinaryOperation;
import org.home.calculator.operation.Operation;

public interface OperationFactory {

    public BinaryOperation createBinaryOperation(char sign);
    public Operation createUnaryOperation(char sign);
}
