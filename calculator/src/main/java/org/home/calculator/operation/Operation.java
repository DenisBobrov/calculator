package org.home.calculator.operation;

import org.home.calculator.evaluation.OperationVisitor;

public interface Operation {

    public void accept(OperationVisitor visitor);
}
