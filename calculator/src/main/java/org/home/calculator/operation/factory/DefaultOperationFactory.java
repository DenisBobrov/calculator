package org.home.calculator.operation.factory;

import org.home.calculator.operation.*;
import org.home.calculator.operation.binary.*;

import java.util.HashMap;
import java.util.Map;

public class DefaultOperationFactory implements OperationFactory {

    private Map<Character, BinaryOperation> binaryOperations;

    public DefaultOperationFactory() {
        initializeOperations();
    }

    @Override
    public BinaryOperation createBinaryOperation(char sign) {
        return binaryOperations.get(sign);
    }

    @Override
    public Operation createUnaryOperation(char sign) {
        throw new IllegalStateException("Not implemented yet.");
    }

    private void initializeOperations() {
        binaryOperations = new HashMap<Character, BinaryOperation>() {
            { put('+', new Addition()); }
            { put('-', new Subtraction()); }
            { put('/', new Division()); }
            { put('*', new Multiplication()); }
            { put('^', new Exponentiation()); }
        };
    }
}
