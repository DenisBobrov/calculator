package org.home.calculator.stateMachine.math;

import org.home.calculator.stateMachine.TransitionMatrix;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import static org.home.calculator.stateMachine.math.MathState.*;

public class MathTransitionMatrix implements TransitionMatrix {

    private Map<MathState, Set<MathState>> transitionMatrix;

    public MathTransitionMatrix() {
        this.transitionMatrix = new HashMap<MathState, Set<MathState>>() {
            { put(START, EnumSet.of(NUMBER, OPENING_BRACKET, FUNCTION)); }
            { put(NUMBER, EnumSet.of(BINARY_OPERATION, CLOSING_BRACKET, FUNCTION_ARGUMENT_SEPARATOR)); }
            { put(BINARY_OPERATION, EnumSet.of(NUMBER, OPENING_BRACKET, FUNCTION)); }
            { put(OPENING_BRACKET, EnumSet.of(NUMBER, OPENING_BRACKET, FUNCTION)); }
            { put(CLOSING_BRACKET, EnumSet.of(CLOSING_BRACKET, BINARY_OPERATION, FUNCTION_ARGUMENT_SEPARATOR)); }
            { put(FUNCTION, EnumSet.of(OPENING_BRACKET)); }
            { put(FUNCTION_ARGUMENT_SEPARATOR, EnumSet.of(NUMBER, OPENING_BRACKET, FUNCTION)); }
        };
    }

    @Override
    public Enum getInitialState() {
        return START;
    }

    @Override
    public Set getFinalStates() {
        return EnumSet.of(NUMBER, CLOSING_BRACKET);
    }

    @Override
    public Set getPossibleStates(Enum state) {
        return transitionMatrix.get(state);
    }
}
