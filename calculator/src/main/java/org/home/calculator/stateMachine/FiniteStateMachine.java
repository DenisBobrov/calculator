package org.home.calculator.stateMachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

abstract public class FiniteStateMachine<State extends Enum> {

    private final Logger logger = LoggerFactory.getLogger(FiniteStateMachine.class);

    private State currentState;
    private Set<State> finiteStates;

    protected FiniteStateMachine() {
        this.currentState = getTransitionMatrix().getInitialState();
        this.finiteStates = getTransitionMatrix().getFinalStates();
    }

    protected boolean isAccepted(State newState) {
        return getTransitionMatrix().getPossibleStates(currentState).contains(newState);
    }

    public void acceptNewState(State newState) throws InvalidStateException {
        if (logger.isInfoEnabled()) {
            logger.info(String.format("Accept new state '%s'", newState.toString()));
        }

        if (isAccepted(newState)) {
            currentState = newState;
        } else {
            String message = String.format(
                    "Malformed expression. After '%s' can be '%s'. '%s' given.",
                    currentState,
                    getTransitionMatrix().getPossibleStates(currentState).toString(),
                    newState
            );

            if (logger.isErrorEnabled()) {
                logger.error(message);
            }

            throw new InvalidStateException(message);
        }
    }

    public boolean isFiniteSate(State state) {
        return finiteStates.contains(state);
    }

    public State getCurrentState() {
        return currentState;
    }

    abstract public TransitionMatrix<State> getTransitionMatrix();
}
