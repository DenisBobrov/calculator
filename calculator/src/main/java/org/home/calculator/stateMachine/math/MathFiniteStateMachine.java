package org.home.calculator.stateMachine.math;

import org.home.calculator.stateMachine.FiniteStateMachine;
import org.home.calculator.stateMachine.TransitionMatrix;

public class MathFiniteStateMachine extends FiniteStateMachine {

    @Override
    public TransitionMatrix getTransitionMatrix() {
        return new MathTransitionMatrix();
    }
}
