package org.home.calculator.stateMachine;

import java.util.Set;

public interface TransitionMatrix<State extends Enum> {

    public State getInitialState();

    public Set<State> getFinalStates();

    public Set<State> getPossibleStates(State state);
}
