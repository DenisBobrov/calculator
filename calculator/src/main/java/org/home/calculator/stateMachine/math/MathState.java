package org.home.calculator.stateMachine.math;

public enum MathState {
    START,
    NUMBER,
    BINARY_OPERATION,
    OPENING_BRACKET,
    CLOSING_BRACKET,
    FUNCTION,
    FUNCTION_ARGUMENT_SEPARATOR
}
