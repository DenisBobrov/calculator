package org.home.calculator;

public class CalculationException extends RuntimeException {

    private int position;

    public CalculationException(String message, int position) {
        super(message);

        this.position = position;
    }

    public CalculationException(String message) {
        this(message, -1);
    }

    public int getPosition() {
        return position;
    }

    public boolean isPositionSupported() {
        return position >= 0;
    }
}
