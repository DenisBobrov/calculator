package org.home.calculator;

public interface Calculator {

    public double calculate(String expression) throws CalculationException;
}
