package org.home.calculator.evaluation;

import org.home.calculator.function.Function;
import org.home.calculator.operation.binary.BinaryOperation;
import org.home.calculator.operation.Operation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class EvaluationContext {
    private Deque<Double> numbers;
    private Deque<Operation> operations;

    private Function function;
    private List<Double> functionArguments;

    private BinaryOperation prevBinaryOperation;

    public BinaryOperation getPrevBinaryOperation() {
        return prevBinaryOperation;
    }

    public void setPrevBinaryOperation(BinaryOperation prevBinaryOperation) {
        this.prevBinaryOperation = prevBinaryOperation;
    }

    public EvaluationContext() {
        numbers = new ArrayDeque<Double>();
        operations = new ArrayDeque<Operation>();

        functionArguments = new ArrayList<Double>();
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public void addFunctionArgument(double argument) {
        functionArguments.add(argument);
    }

    public List<Double> getFunctionArguments() {
        return functionArguments;
    }

    public void pushNumber(double number) {
        numbers.push(number);
    }

    public double peekNumber() {
        return numbers.peek();
    }

    public double popNumber() {
        return numbers.pop();
    }

    public void pushOperation(Operation operation) {
        operations.push(operation);
    }

    public Operation peekOperation() {
        return operations.peek();
    }

    public Operation popOperation() {
        return operations.pop();
    }

    public boolean isEmptyOperations() {
        return operations.isEmpty();
    }

    public boolean isEmptyNumbers() {
        return numbers.isEmpty();
    }

    public int getSizeOperations() {
        return operations.size();
    }

    public int getSizeNumbers() {
        return numbers.size();
    }

    public boolean isFunctionContext() {
        return function != null;
    }
}
