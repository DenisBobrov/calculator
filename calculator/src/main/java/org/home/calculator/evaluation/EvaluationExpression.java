package org.home.calculator.evaluation;

import org.home.calculator.CalculationException;
import org.home.calculator.function.Function;
import org.home.calculator.function.factory.FunctionFactory;
import org.home.calculator.operation.*;
import org.home.calculator.operation.binary.Association;
import org.home.calculator.operation.binary.BinaryOperation;
import org.home.calculator.operation.factory.OperationFactory;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class EvaluationExpression {

    private OperationFactory operationFactory;
    private FunctionFactory functionFactory;

    private Deque<EvaluationContext> evaluationContextDeque;
    private Function function;

    public EvaluationExpression(OperationFactory operationFactory, FunctionFactory functionFactory) {
        this.operationFactory = operationFactory;
        this.functionFactory = functionFactory;

        evaluationContextDeque = new ArrayDeque<EvaluationContext>();
        evaluationContextDeque.push(new EvaluationContext());
    }

    public EvaluationContext getEvaluationContext() {
        return evaluationContextDeque.peek();
    }

    public void addEvaluationContext() {
        EvaluationContext newContext = new EvaluationContext();

        if (function != null) {
            newContext.setFunction(function);

            function = null;
        }

        evaluationContextDeque.push(newContext);
    }

    public void removeEvaluationContext() {
        if (!isChildEvaluationContext()) {
            throw new CalculationException("Cannot evaluate and remove evaluation context.");
        }

        evaluate();

        double result = getResult();

        evaluationContextDeque.pop();

        getEvaluationContext().pushNumber(result);
    }

    public void addFunction(Function function) {
        this.function = function;
    }

    public void addFunctionArgument() {
        evaluate();

        EvaluationContext context = getEvaluationContext();

        context.getFunctionArguments().add(context.popNumber());
    }

    public void addBinaryOperation(BinaryOperation operation) {
        // try to calculate expression
        BinaryOperation prevOperation = getEvaluationContext().getPrevBinaryOperation();

        if (prevOperation != null) {
            if (prevOperation.getAssociation() == Association.LEFT && operation.getAssociation() == Association.LEFT) {
                if (prevOperation.getPrecedence() >= operation.getPrecedence()) {
                    evaluateOperation();
                }
            } else if (prevOperation.getAssociation() == Association.RIGHT && operation.getAssociation() == Association.LEFT) {
                if (prevOperation.getPrecedence() >= operation.getPrecedence()) {
                    evaluateOperation();
                }
            }
        }

        getEvaluationContext().setPrevBinaryOperation(operation);
        getEvaluationContext().pushOperation(operation);
    }

    public OperationFactory getOperationFactory() {
        return operationFactory;
    }

    public FunctionFactory getFunctionFactory() {
        return functionFactory;
    }

    private void evaluateOperation() {
        Operation operation =  getEvaluationContext().popOperation();

        operation.accept(new EvaluationVisitor(getEvaluationContext()));
    }

    public void evaluate() {
        while (!getEvaluationContext().isEmptyOperations()) {
            evaluateOperation();
        }
    }

    public double getResult() {
        if (isEvaluationDone()) {
            EvaluationContext context = getEvaluationContext();

            double result = getEvaluationContext().peekNumber();

            if (context.isFunctionContext()) {
                // add last argument
                List<Double> functionArguments = getEvaluationContext().getFunctionArguments();
                functionArguments.add(result);

                Double[] arguments = new Double[functionArguments.size()];
                result = getEvaluationContext().getFunction().calculate(functionArguments.toArray(arguments));
            }

            return result;
        }

        throw new CalculationException("Malformed expression.");
    }

    private boolean isEvaluationDone () {
        return (getEvaluationContext().isEmptyOperations() && getEvaluationContext().getSizeNumbers() == 1);
    }

    public boolean isChildEvaluationContext() {
        return evaluationContextDeque.size() > 1;
    }
}
