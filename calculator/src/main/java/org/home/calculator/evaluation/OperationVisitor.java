package org.home.calculator.evaluation;

import org.home.calculator.operation.binary.BinaryOperation;

public interface OperationVisitor {
    public void visit(BinaryOperation operation);
}
