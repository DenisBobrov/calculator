package org.home.calculator.evaluation;

import org.home.calculator.operation.binary.BinaryOperation;

public class EvaluationVisitor implements OperationVisitor {

    private EvaluationContext context;

    public EvaluationVisitor(EvaluationContext context) {
        this.context = context;
    }

    @Override
    public void visit(BinaryOperation operation) {
        double rightOperand = context.popNumber();
        double leftOperand = context.popNumber();

        context.pushNumber(operation.calculate(leftOperand, rightOperand));
    }
}
