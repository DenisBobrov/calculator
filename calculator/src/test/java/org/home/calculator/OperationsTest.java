package org.home.calculator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class OperationsTest {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new MathCalculator();
    }

    @Test
    public void testAdditionOperation() {
        assertEquals("Addition operation of 5+6 failed result.", calculator.calculate("5+6"), 11.0, 0);
        assertEquals("Addition operation of 5+99+93.5+11+0+97 failed result.", calculator.calculate("5+99+93.5+11+0+97"), 305.5, 0);
        assertEquals("Addition operation of 5+6+11 failed result.", calculator.calculate("5+6+11"), 22.0, 0);
        assertEquals("Addition operation of 5+698 failed result.", calculator.calculate("5+698"), 703.0, 0);
    }

    @Test
    public void testSubtractionOperation() {
        assertEquals("Subtraction operation of 5-6 failed result.", calculator.calculate("5-6"), -1.0, 0);
        assertEquals("Subtraction operation of 11-9-2 failed result.", calculator.calculate("11-9-2"), 0.0, 0);
        assertEquals("Subtraction operation of 100-5-69-2 failed result.", calculator.calculate("100-5-69-2"), 24.0, 0);
        assertEquals("Subtraction operation of 11-1-1-1-1 failed result.", calculator.calculate("11-1-1-1-1"), 7.0, 0);
    }

    @Test
    public void testMultiplicationOperation() {
        assertEquals("Multiplication operation 11*2 failed result.", calculator.calculate("11*2"), 22.0, 0);
        assertEquals("Multiplication operation 0*0 failed result.", calculator.calculate("0*0"), 0.0, 0);
        assertEquals("Multiplication operation 5.5*7.1*2 failed result.", calculator.calculate("5.5*7.1*2"), 78.1, 0);
        assertEquals("Multiplication operation 0.1*.023*599*2 failed result.", calculator.calculate("0.1*.023*599*2"), 2.7554, 0);
    }

    @Test
    public void testDivisionOperation() {
        assertEquals("Division operation 11/2 failed result.", calculator.calculate("11/2"), 5.5, 0);
        assertEquals("Division operation 5/0 failed result.", calculator.calculate("5/0"), Double.POSITIVE_INFINITY, 0);
        assertEquals("Division operation 90/11/89/56.3 failed result.", calculator.calculate("90/11/89/56.3"), 0.0016328692960700464, 0);
        assertEquals("Division operation 6985/121/2 failed result.", calculator.calculate("6985/121/2"), 28.863636363636363, 0);
    }
}
