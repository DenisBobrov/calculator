package org.home.calculator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class PrecedenceTest {
    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new MathCalculator();
    }

    @Test
    public void testPrecedence() {
        assertEquals("Precedence test of 5*3+1 failed result.", calculator.calculate("5*3+1"), 16.0, 0);
        assertEquals("Precedence test of 98/9*12-54/98*12 failed result.", calculator.calculate("98/9*12-54/98*12"), 124.0544217687075, 0);
        assertEquals("Precedence test of 11/2/3*9 failed result.", calculator.calculate("11/2/3*9"), 16.5, 0);
        assertEquals("Precedence test of 5+5-6-11+2+47 failed result.", calculator.calculate("5+5-6-11+2+47"), 42.0, 0);
        assertEquals("Precedence test of 98-8-3-120-12+88-12 failed result.", calculator.calculate("98-8-3-120-12+88-12"), 31.0, 0);
        assertEquals("Precedence test of 12*55-45+332+89/45/12*8 failed result.", calculator.calculate("12*55-45+332+89/45/12*8"), 948.3185185185185, 0);
    }
}
