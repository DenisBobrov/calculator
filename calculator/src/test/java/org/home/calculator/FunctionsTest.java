package org.home.calculator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class FunctionsTest {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new MathCalculator();
    }

    @Test
    public void testSqrtFunction() {
        assertEquals("Sqrt function test of sqrt(9) failed result.", calculator.calculate("sqrt(9)"), 3.0, 0);
        assertEquals("Sqrt function test of sqrt(11-(2*3)) failed result.", calculator.calculate("sqrt(11-(2*3))"), 2.23606797749979, 0);
        assertEquals("Sqrt function test of sqrt(12*55-45+332+89/45/12*8) failed result.", calculator.calculate("sqrt(12*55-45+332+89/45/12*8)"), 30.79478070255605, 0);
        assertEquals("Sqrt function test of sqrt((2+2)*11*(11-1+56+22)/(11-(33-(44*2-(11-3))))) failed result.", calculator.calculate("sqrt((2+2)*11*(11-1+56+22)/(11-(33-(44*2-(11-3)))))"), 8.170594879790283, 0);
    }

    @Test
    public void testSqrtFunctionExceptions() {
        try {
            calculator.calculate("sqrt(0-1)");
            fail("Expected an CalculationException to be thrown. Negative value given for sqrt function.");
        } catch (CalculationException ex) {
            assertEquals(ex.getMessage(), "Cannot calculate sqrt of negative value -1.0.");
        }

        try {
            calculator.calculate("sqrt(9,3)");
            fail("Expected an CalculationException to be thrown. 2 arguments given for sqrt function.");
        } catch (CalculationException ex) {
            assertEquals(ex.getMessage(), "Sqrt function expects only one argument. 2 given.");
        }
    }

    @Test
    public void testMinFunction() {
        assertEquals("Min function test of min(1,2) failed result.", calculator.calculate("min(1,2)"), 1.0, 0);
        assertEquals("Min function test of min(100-11*2, 145,(33-1)) failed result.", calculator.calculate("min(100-11*2, 145,(33-1))"), 32.0, 0);
        assertEquals("Min function test of min((((33-2)*41)-(11-25*2)), 2000) failed result.", calculator.calculate("min((((33-2)*41)-(11-25*2)), 2000)"), 1310.0, 0);
    }

    @Test
    public void testMinFunctionExceptions() {
        try {
            calculator.calculate("min(1)");
            fail("Expected an CalculationException to be thrown. Not enough arguments for min arguments.");
        } catch (CalculationException ex) {
            assertEquals(ex.getMessage(), "Min function expects at least 2 (two) arguments. 1 argument(s) given.");
        }
    }

    @Test
    public void testMaxFunction() {
        assertEquals("Max function test of max(1,2) failed result.", calculator.calculate("max(1,2)"), 2.0, 0);
        assertEquals("Max function test of max(100-11*2, 145,(33-1)) failed result.", calculator.calculate("max(100-11*2, 145,(33-1))"), 145.0, 0);
        assertEquals("Max function test of max((((33-2)*41)-(11-25*2)), 2000) failed result.", calculator.calculate("max((((33-2)*41)-(11-25*2)), 2000)"), 2000, 0);
    }

    @Test
    public void testMaxFunctionExceptions() {
        try {
            calculator.calculate("max(1)");
            fail("Expected an CalculationException to be thrown. Not enough arguments for max arguments.");
        } catch (CalculationException ex) {
            assertEquals(ex.getMessage(), "Max function expects at least 2 (two) arguments. 1 argument(s) given.");
        }
    }

    @Test
    public void testSumFunction() {
        assertEquals("Sum function test of sum(1,2) failed result.", calculator.calculate("sum(1,2)"), 3.0, 0);
        assertEquals("Sum function test of sum(100-11*2, 145,(33-1)) failed result.", calculator.calculate("sum(100-11*2, 145,(33-1))"), 255, 0);
        assertEquals("Sum function test of sum((((33-2)*41)-(11-25*2)), 2000) failed result.", calculator.calculate("sum((((33-2)*41)-(11-25*2)), 2000)"), 3310, 0);
    }

    @Test
    public void testSumFunctionExceptions() {
        try {
            calculator.calculate("sum(1)");
            fail("Expected an CalculationException to be thrown. Not enough arguments for sum arguments.");
        } catch (CalculationException ex) {
            assertEquals(ex.getMessage(), "Sum function expects at least 2 (two) arguments. 1 argument(s) given.");
        }
    }

    @Test
    public void testFunctionInFunction() {
        assertEquals("Function in function test of min(min(13,44),min(11-2, 9, min(11,12))) failed result.", calculator.calculate("min(min(13,44),min(11-2, 9, min(11,12)))"), 9.0, 0);
        assertEquals("Function in function test of sum(min(6,5,3), max(11,25)) failed result.", calculator.calculate("sum(min(6,5,3), max(11,25))"), 28.0, 0);
        assertEquals("Function in function test of max(sum(1,2,3,min(11,2)),2) failed result.", calculator.calculate("max(sum(1,2,3,min(11,2)),2)"), 8.0, 0);
    }
}
