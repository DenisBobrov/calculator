package org.home.calculator;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BracketsTest {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new MathCalculator();
    }

    @Test
    public void testBrackets() {
        assertEquals("Brackets test of (5-3)*2 failed result.", calculator.calculate("(5-3)*2"), 4.0, 0);
        assertEquals("Brackets test of 11*(5-96)*22/(1+3+11*5/(445-665)) failed result.", calculator.calculate("11*(5-96)*22/(1+3+11*5/(445-665))"), -5872.533333333334, 0);
        assertEquals("Brackets test of (458*(121-965))+(989*4212-11.11)+(4545-4-4-556+6565*8)/81/5 failed result.", calculator.calculate("(458*(121-965))+(989*4212-11.11)+(4545-4-4-556+6565*8)/81/5"), 3779244.3986419756, 0);
        assertEquals("Brackets test of (2+2)*11*(11-1+56+22)/(11-(33-(44*2-(11-3)))) failed result.", calculator.calculate("(2+2)*11*(11-1+56+22)/(11-(33-(44*2-(11-3))))"), 66.75862068965517, 0);
    }

    @Test
    public void testMismatchedOpeningBracketsExceptions() {
        try {
            calculator.calculate("(5+3))*2");
            fail("Expected an CalculationException to be thrown. Mismatched opening brackets in (5+3))*2.");
        } catch (CalculationException ex) {
            assertEquals(ex.getPosition(), 5);
        }

        try {
            calculator.calculate("(2))-11");
            fail("Expected an CalculationException to be thrown. Mismatched opening brackets in (2))-11.");
        } catch (CalculationException ex) {
            assertEquals(ex.getPosition(), 3);
        }

        try {
            calculator.calculate("(11-1))*(22+8)/(13-2)");
            fail("Expected an CalculationException to be thrown. Mismatched opening brackets in 11-1))*(22+8)/(13-2).");
        } catch (CalculationException ex) {
            assertEquals(ex.getPosition(), 6);
        }

        try {
            calculator.calculate("6+(5");
            fail("Expected an CalculationException to be thrown. Mismatched closing brackets in 6+(5.");
        } catch (CalculationException ex) {
            assertEquals(ex.getMessage(), "Mismatched closing brackets.");
        }

        try { //
            calculator.calculate("(11-1)*(22+(8)/(13-2)");
            fail("Expected an CalculationException to be thrown. Mismatched closing brackets in (11-1)*(22+(8)/(13-2)");
        } catch (CalculationException ex) {
            assertEquals(ex.getMessage(), "Mismatched closing brackets.");
        }
    }
}
