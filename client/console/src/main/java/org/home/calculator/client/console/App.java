package org.home.calculator.client.console;

import org.home.calculator.CalculatorFactory;
import org.home.calculator.CalculationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter math expression:");

        try {
            String expression = reader.readLine();

            System.out.println(CalculatorFactory.createMathCalculator().calculate(expression));
        } catch (CalculationException ce) {
            System.out.println("Error: " + ce.getMessage());
        } catch (IOException ioe) {
//           handle exception
        } catch (Exception ex) {
            System.out.println("Unexpected error: " + ex.getMessage());
        }
    }
}
