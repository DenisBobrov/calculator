package org.home.calculator.client.gui.frame;

import org.home.calculator.CalculatorFactory;
import org.home.calculator.CalculationException;
import org.home.calculator.parser.ParseException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainFrame extends JFrame {

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new MainFrame();

                frame.pack();
                frame.setVisible(true);
            }
        });
    }

    private JLabel lblExpression;
    private JLabel lblResult;
    private JLabel lblError;
    private JTextField txtExpression;
    private JTextField txtResult;
    private JButton btnCalculate;

    public MainFrame() throws HeadlessException {

        setTitle("Math calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        initializeComponents();
    }

    protected void onActionPerformedTxtExpression(ActionEvent e) {
        onActionPerformedBtnCalculate(e);
    }

    protected void onActionPerformedBtnCalculate(ActionEvent e) {
        lblError.setText(" ");

        try {
            double r = calculate(txtExpression.getText());

            txtResult.setText(String.valueOf(r));
        } catch (CalculationException ex) {
            lblError.setText(ex.getMessage());

            if (ex.isPositionSupported()) {
                txtExpression.setCaretPosition(ex.getPosition());
            }
        } catch (ParseException ex) {
            lblError.setText("Malformed expression.");
            txtExpression.setCaretPosition(ex.getPosition());
        } catch (Exception ex) {
            lblError.setText("Malformed expression.");
        }

        txtExpression.requestFocus();
    }

    private void initializeComponents() {
        GridBagConstraints c = new GridBagConstraints();
        Container pane = getContentPane();

        pane.setLayout(new GridBagLayout());

        lblExpression = new JLabel("Expression:");
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 0;
        pane.add(lblExpression, c);

        txtExpression = new JTextField();
        txtExpression.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onActionPerformedTxtExpression(e);
            }
        });
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipadx = 250;
        c.gridx = 1;
        c.gridy = 0;
        pane.add(txtExpression, c);

        lblResult = new JLabel("Result:");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipadx = 0;
        c.gridx = 0;
        c.gridy = 1;
        pane.add(lblResult, c);

        txtResult = new JTextField();
        txtResult.setEditable(false);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipadx = 250;
        c.gridx = 1;
        c.gridy = 1;
        pane.add(txtResult, c);

        lblError = new JLabel(" ");
        lblError.setForeground(Color.RED);
        c.fill = GridBagConstraints.NONE;
        c.ipadx = 0;
        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 2;
        pane.add(lblError, c);

        btnCalculate = new JButton("Calculate");
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onActionPerformedBtnCalculate(e);
            }
        });
        c.anchor = GridBagConstraints.LINE_END;
        c.fill = GridBagConstraints.NONE;
        c.weighty = 1;
        c.ipadx = 50;
        c.gridx = 0;
        c.gridy = 3;
        pane.add(btnCalculate, c);
    }

    private double calculate(String expression) throws CalculationException{
        return CalculatorFactory.createMathCalculator().calculate(expression);
    }
}
