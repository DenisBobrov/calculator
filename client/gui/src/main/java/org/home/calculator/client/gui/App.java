package org.home.calculator.client.gui;

import org.apache.commons.cli.*;
import org.home.calculator.client.gui.frame.MainFrame;

import javax.swing.*;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        Options options = new Options();

        options.addOption("c", "console", false, "run from console");

        CommandLineParser parser = new PosixParser();

        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.hasOption('c')) {
                org.home.calculator.client.console.App.main(args);
            } else {
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        JFrame frame = new MainFrame();

                        frame.pack();
                        frame.setVisible(true);
                    }
                });
            }
        } catch (ParseException e) {
            System.out.println("Unexpected error: " + e.getMessage());
        }
    }
}
